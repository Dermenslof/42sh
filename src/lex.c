#include <stdlib.h>
#include "msh.h"

static int		g_transition[2][5] = {
	{0, 1, 1, 0, S_END},
	{0, S_ERR(0), S_ERR(0), S_ERR(2), S_ERR(1)}
};

static char		*g_errors[] = {
	"Unexpected operator.",
	"Unexpected end of line.",
	"Unexpected semicolon."
};

t_tok			*ft_tok_alloc(void)
{
	return (ft_memalloc(sizeof(t_tok)));
}

static t_tok	*ft_tokenize(t_tok *tok, t_token_type t)
{
	if (tok)
		tok->type = t;
	return (tok);
}

static t_tok	*ft_grab_token(char **str, t_sh *shell)
{
	t_tok		*tok;

	tok = ft_tokenize(ft_grab_end(str), T_END);
	if (!tok)
		tok = ft_tokenize(ft_grab_operator(str), T_OPERATOR);
	if (!tok)
		tok = ft_tokenize(ft_grab_pipe(str, shell), T_PIPE);
	if (!tok)
		tok = ft_tokenize(ft_grab_sep(str), T_SEP);
	if (!tok)
		tok = ft_tokenize(ft_grab_operand(str, shell), T_OPERAND);
	return (tok);
}

int				lex(char **str, t_tlist *list, t_sh *shell)
{
	int			state;
	t_tok		*tok;

	state = 0;
	while (state != S_END && state < S_ERR(0))
	{
		while (**str == ' ')
			(*str)++;
		tok = ft_grab_token(str, shell);
		ft_tlist_push_back(list, (t_tlist_node *)tok);
		state = g_transition[state][(int)tok->type];
	}
	if (state >= S_ERR(0))
	{
		ft_putstr_fd("42sh: Error: ", 2);
		ft_putendl_fd(g_errors[state - S_ERR(0)], 2);
		return (0);
	}
	return (1);
}
