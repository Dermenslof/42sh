#include <stdlib.h>
#include "msh.h"

char		*ft_wind(char *str)
{
	char	*buf;

	buf = ft_memalloc(ft_strlen(str) + 2);
	buf++;
	ft_strcpy(buf, str);
	free(str);
	return (buf);
}

void		ft_unwind(char *str)
{
	int		i;

	i = -1;
	while (str[i])
		i--;
	free(str + i);
}
