#include <signal.h>
#include "msh.h"

t_sh			g_shell;

int				main(int argc, char **argv)
{
	(void)argv;
	(void)argc;
	ft_bzero(&g_shell, sizeof(g_shell));
	meta_init(&g_shell);
	ft_main_loop(&g_shell);
	meta_init_revert(&g_shell);
	return (g_shell.exit_code);
}
