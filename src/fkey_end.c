#include <term.h>
#include "msh.h"

void			fkey_end(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	int			co;

	(void)sh;
	(void)inbuf;
	co = tgetnum("co");
	while (ppt->idx < ppt->mx)
	{
		ppt->idx++;
		if (iterate_hpos(ppt, 1, co) == 0)
			tputs(tgetstr("nd", NULL), 0, ft_termput);
	}
}
