#include <unistd.h>
#include <signal.h>
#include <term.h>
#include <fcntl.h>
#include <stdlib.h>
#include "msh.h"

/*
** Matrix like terminal-saver
*/

static int				g_run;

static void				print_matrix(int fd)
{
	unsigned char	random;

	if (read(fd, &random, sizeof(unsigned char)) < 1)
		return ;
	if (random < 30)
		write(1, "0", 1);
	else if (random > 30 && random < 60)
		write(1, "1", 1);
	else
		write(1, " ", 1);
	if (random == 49)
		write(1, "Meow", 4);
	if (random == 51)
		write(1, "Miaou", 5);
	ft_usleep(500);
}

static void				local_handler(int a)
{
	(void)a;
	g_run = 1;
}

static void				restore(int fd, t_sh *shell)
{
	close(fd);
	g_run = 0;
	ft_putstr("\033[0m\n");
	tputs(tgetstr("ve", NULL), 0, ft_termput);
	meta_init_redo(shell);
}

static void				init(t_sh *shell, int *fd)
{
	struct termios		tmp;

	if ((*fd = open("/dev/urandom", O_RDONLY)) < 0)
		exit(1);
	g_run = 0;
	tmp = shell->term.sh;
	tmp.c_lflag = tmp.c_lflag | ISIG;
	tcsetattr(0, TCSAFLUSH, &tmp);
	meta_init_revert(shell);
	signal(SIGINT, local_handler);
	tputs(tgetstr("vi", NULL), 0, ft_termput);
	ft_putstr("\033[32m");
	ft_putstr("Exit this wit' ctrl+C\n");
}

int						builtin_matrix(t_sh *shell, int ac, char **av)
{
	int					fd;
	int					tim;

	(void)shell;
	tim = 1;
	if (ac > 2)
	{
		ft_putendl_fd("Error: usage: matrix [sleeptime]", 2);
		return (1);
	}
	if (ac == 2)
	{
		tim = ft_atoi(av[1]);
		tim = (tim < 0) ? -tim : tim;
		tim *= 2000;
	}
	init(shell, &fd);
	while (tim-- && !g_run)
	{
		if (ac == 1)
			tim++;
		print_matrix(fd);
	}
	restore(fd, shell);
	return (0);
}
