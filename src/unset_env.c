#include "msh.h"

static char		*g_key;

static int		is_key(t_tlist_node *node)
{
	if (ft_strcmp(g_key, ((t_var *)node)->key) == 0)
		return (1);
	return (0);
}

int				builtin_unsetenv(t_sh *shell, int ac, char **av)
{
	int		i;

	i = 1;
	if (ac == 1)
	{
		ft_putendl("unsetenv: too few arguments.");
		return (1);
	}
	while (av[i])
	{
		g_key = av[i];
		ft_tlist_remove_if(shell->env, is_key, NULL);
		i++;
	}
	return (0);
}
