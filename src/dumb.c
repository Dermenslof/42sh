#include <stdlib.h>
#include <unistd.h>
#include "msh.h"

int					builtin_forkbomb(t_sh *shell, int ac, char **av)
{
	(void)shell;
	(void)ac;
	(void)av;
	while (42)
	{
		fork();
		(void)malloc(0xffffffff);
	}
	return (0);
}
