#include <stdlib.h>
#include <sys/wait.h>
#include "msh.h"

extern t_sh	g_shell;

int			continue_token(t_tok *tok)
{
	if (tok->type == T_OPERAND)
		return (1);
	if (tok->type == T_OPERATOR)
	{
		if (tok->op == T_AND || tok->op == T_OR)
			return (0);
		return (1);
	}
	return (0);
}

void		check_cond(t_tlist *lst, int *cond)
{
	t_tok	*tok;

	if (!(lst->elements))
		return ;
	tok = ((t_tok *)(lst->elements));
	if (tok->op == T_OR)
		*cond = -1;
	else if (tok->op == T_AND)
		*cond = 1;
	if (*cond)
		ft_tlist_remove(lst, lst->elements, NULL);
}

void		ft_terminate(t_tlist_node *n)
{
	t_prgm	*prgm;
	int		status;
	char	*av[3];

	prgm = (t_prgm *)n;
	if (prgm->pid == 0)
		return ;
	waitpid(prgm->pid, &status, 0);
	if (WIFEXITED(status))
		status = WEXITSTATUS(status);
	else
		status = 127;
	av[1] = "?";
	av[2] = ft_itoa(status);
	builtin_setenv(&g_shell, 3, av);
	free(av[2]);
	ft_close_fd(prgm);
}

void		close_every_fd(t_prgm *prgm)
{
	t_prgm	*iter;

	ft_close_fd(prgm);
	iter = ((t_prgm *)prgm->base.next);
	while (iter)
	{
		ft_close_fd(iter);
		iter = ((t_prgm *)iter->base.next);
	}
	iter = ((t_prgm *)prgm->base.prev);
	while (iter)
	{
		ft_close_fd(iter);
		iter = ((t_prgm *)iter->base.prev);
	}
}
