#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include "msh.h"

static int		is_pwd(t_tlist_node *element)
{
	if (ft_strcmp(((t_var *)element)->key, "PWD") == 0)
		return (1);
	return (0);
}

static int		is_oldpwd(t_tlist_node *element)
{
	if (ft_strcmp(((t_var *)element)->key, "OLDPWD") == 0)
		return (1);
	return (0);
}

static void		update_pwd(t_sh *shell)
{
	t_var	*pwd;
	t_var	*oldpwd;

	free(shell->oldpwd);
	shell->oldpwd = shell->pwd;
	shell->pwd = getcwd(NULL, 0);
	pwd = (t_var *)ft_tlist_search(shell->env, is_pwd);
	oldpwd = (t_var *)ft_tlist_search(shell->env, is_oldpwd);
	free(oldpwd->value);
	oldpwd->value = pwd->value;
	pwd->value = ft_strdup(shell->pwd);
}

static int		go_directory(char *str, t_sh *shell, int status)
{
	struct stat		buf;
	int				flag;

	flag = 0;
	if ((access(str, F_OK) == -1 || stat(str, &buf) == -1) && ++flag)
		ft_putstr("cd : no such or file directory: ");
	else if ((buf.st_mode & S_IFMT) != S_IFDIR && ++flag)
		ft_putstr("cd: not a directory: ");
	else if (chdir(str) == -1 && ++flag)
		ft_putstr("cd: permission denied: ");
	else
		update_pwd(shell);
	if (flag)
		ft_putendl(str);
	if (status)
		free(str);
	return (flag);
}

int				builtin_cd(t_sh *shell, int ac, char **av)
{
	char	**path;
	char	*new_path;

	if (ac == 1)
		return (go_directory(sh_getenv("HOME", shell->env), shell, 0));
	else if (ac == 2 && !ft_strcmp(av[1], "-"))
		return (go_directory(shell->oldpwd, shell, 0));
	else if (ac == 2)
		return (go_directory(av[1], shell, 0));
	else if (ac == 3)
	{
		if ((path = split_path(shell->pwd, av[1], av[2])) == NULL)
		{
			ft_putstr("cd: string not in pwd: ");
			ft_putendl(av[1]);
		}
		else
		{
			new_path = generate_path(path);
			return (go_directory(new_path, shell, 1));
		}
	}
	else
		ft_putendl("cd: too many arguments");
	return (1);
}
