#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include "msh.h"

static t_sh			*g_shell_static;
static int			g_childs;

char				**env_to_arr(t_sh *shell)
{
	char			**env;
	t_var			*var;
	int				i;

	i = 0;
	env = ft_memalloc((ft_tlist_size(shell->env) + 1) * sizeof(char *));
	var = ((t_var *)shell->env->elements);
	while (var)
	{
		env[i] = ft_memalloc(ft_strlen(var->key) + ft_strlen(var->value) + 2);
		ft_strcpy(env[i], var->key);
		ft_strcat(env[i], "=");
		ft_strcat(env[i], var->value);
		var = (t_var *)(var->base.next);
		i++;
	}
	return (env);
}

static int			ft_exec_binary(t_sh *shell, t_prgm *prgm)
{
	if (access(prgm->argv[0], F_OK))
	{
		ft_putendl_fd("Error: No such file or directory.", 2);
		return (127);
	}
	if (access(prgm->argv[0], X_OK))
	{
		ft_putendl_fd("Error: Permission Denied.", 2);
		return (1);
	}
	g_childs++;
	ft_exec_binary2(shell, prgm);
	return (0);
}

static int			ft_exec_builtin(t_sh *shell, t_prgm *prgm)
{
	t_builtin_func	f;
	int				fd[2];
	char			*buf;
	char			*av[3];

	f = lookup_builtin(prgm->argv[0]);
	if (!f)
		return (0);
	fd[0] = dup(0);
	fd[1] = dup(1);
	dup2(prgm->in, 0);
	dup2(prgm->out, 1);
	buf = ft_itoa(f(shell, prgm->argc, prgm->argv));
	av[1] = "?";
	av[2] = buf;
	builtin_setenv(shell, 3, av);
	free(buf);
	dup2(fd[0], 0);
	dup2(fd[1], 1);
	close(fd[0]);
	close(fd[1]);
	ft_close_fd(prgm);
	return (1);
}

static void			ft_exec_each(t_tlist_node *n)
{
	t_prgm			*prgm;
	char			*bin;

	prgm = (t_prgm *)n;
	if (ft_exec_builtin(g_shell_static, prgm))
		return ;
	if (!ft_strchr(prgm->argv[0], '/'))
	{
		bin = prgm->argv[0];
		prgm->argv[0] = ft_find_exec(bin, g_shell_static);
		free(bin);
		if (!prgm->argv[0])
			return ;
	}
	ft_exec_binary(g_shell_static, prgm);
}

void				exec_cmd(t_sh *sh, t_tlist *prgm, int cond)
{
	int				v;

	v = ft_strcmp(sh_getenv("?", sh->env), "0");
	if (cond == -1 && v == 0)
		return ;
	else if (cond == 1 && v != 0)
		return ;
	tcsetattr(0, TCSAFLUSH, &sh->term.sh);
	g_shell_static = sh;
	ft_link_pipe(prgm);
	ft_tlist_each(prgm, ft_exec_each);
	ft_tlist_each(prgm, ft_terminate);
	meta_init_redo(sh);
}
