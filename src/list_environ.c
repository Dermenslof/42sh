#include <stdlib.h>
#include "msh.h"

void				add_last_ret(t_sh *shell)
{
	char	*ret[3];

	(void)shell;
	ret[0] = "";
	ret[1] = "?";
	ret[2] = "0";
	builtin_setenv(shell, 3, ret);
}

int					list_environ(t_tlist **p_env)
{
	extern char		**environ;
	int				i;
	int				re;
	t_var			var;
	t_var			*lnk;

	if (!(*p_env = recall_env_list(RECALL_GET)))
		return (-1);
	i = 0;
	while (environ[i])
	{
		ft_bzero(&var, sizeof(var));
		if ((re = fill_var(environ[i], &var)) < 0)
			return (-1);
		else if (re == 0)
		{
			if (!(lnk = (t_var *)malloc(sizeof(t_var))))
				return (-1);
			ft_memmove(lnk, &var, sizeof(t_var));
			ft_tlist_push_back(*p_env, (t_tlist_node *)lnk);
		}
		i += 1;
	}
	return (0);
}
