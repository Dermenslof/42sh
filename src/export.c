#include <stdlib.h>
#include "msh.h"

static void		apply_export(t_sh *shell, char *key, char *value)
{
	t_var	*element;

	*value = 0;
	value++;
	element = search_env(key, shell->env);
	if (element != NULL)
	{
		free(element->value);
		element->value = ft_strdup(value);
	}
	else
	{
		element = create_var(key, value);
		ft_tlist_push_back(shell->env, (t_tlist_node *)element);
	}
}

int				builtin_export(t_sh *shell, int ac, char **av)
{
	char	*value;
	int		i;

	i = 1;
	if (ac == 1)
		return (builtin_env(shell, ac, av));
	while (av[i])
	{
		value = ft_strchr(av[i], '=');
		if (value != NULL)
			apply_export(shell, av[i], value);
		i++;
	}
	return (0);
}
