#include <stdlib.h>
#include "msh.h"

int			i_update_environ(t_sh *sh)
{
	t_var	*el;
	int		lvl;
	char	*proav[4];

	proav[0] = "setenv";
	proav[1] = "SHLVL";
	proav[3] = NULL;
	lvl = 1;
	if ((el = search_env(proav[1], sh->env)))
		lvl += ft_atoi(el->value);
	if (!(proav[2] = ft_itoa(lvl)))
		return (-1);
	lvl = builtin_setenv(sh, 3, proav);
	free(proav[2]);
	return (lvl);
}
