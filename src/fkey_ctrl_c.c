#include <term.h>
#include "msh.h"

void			fkey_ctrl_c(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	(void)sh;
	(void)inbuf;
	hist2_trigger(sh, ppt);
	ppt->flag |= DISCARD_BIT;
	ft_putchar('\n');
	tputs(tgetstr("ce", NULL), 1, ft_termput);
}
