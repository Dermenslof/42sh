#include <unistd.h>
#include "msh.h"

int		builtin_echo(t_sh *shell, int ac, char **av)
{
	int		i;
	int		flag;

	i = 1;
	flag = 0;
	(void)shell;
	if (ac == 1)
	{
		write(1, "\n", 1);
		return (0);
	}
	while (av[i] && !ft_strcmp("-n", av[i]) && ++flag)
		i++;
	while (av[i])
	{
		ft_putstr(av[i]);
		if (av[i + 1] != NULL)
			write(1, " ", 1);
		else if (!flag)
			write(1, "\n", 1);
		i++;
	}
	return (0);
}
