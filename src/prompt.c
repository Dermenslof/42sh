#include <stdlib.h>
#include <unistd.h>
#include <term.h>
#include <sys/ioctl.h>
#include "msh.h"

static t_ppt		*g_prompt_static;

static struct s_key	g_fkeyfuncs[E_LEN] =
{
	{SQ_NOP, fkey_nop},
	{SQ_CTRL_C, fkey_ctrl_c},
	{SQ_CTRL_D, fkey_ctrl_d},
	{SQ_RETURN, fkey_return},
	{SQ_BACKSP, fkey_backsp},
	{SQ_BS, fkey_backsp},
	{SQ_DEL, fkey_del},
	{SQ_HOME, fkey_home},
	{SQ_SOH, fkey_home},
	{SQ_END, fkey_end},
	{SQ_UP, fkey_up},
	{SQ_DOWN, fkey_down},
	{SQ_LEFT, fkey_left},
	{SQ_RIGHT, fkey_right},
};

void				prompt_sigint_handler(int foo)
{
	foo ^= foo;
	*(char *)&foo = '\003';
	if (g_prompt_static)
	{
		g_prompt_static->flag |= DISCARD_BIT;
	}
	ioctl(0, TIOCSTI, (char *)&foo);
}

static void			f_handle_flag(t_sh *sh, t_ppt *ppt)
{
	ft_strdel(&ppt->line);
	if (ppt->flag & 4)
		internal_exit_wrapper(sh, 0);
}

void				put_prompt(char *prompt, t_ppt *ppt)
{
	ppt->prompt = prompt;
	smart_put(ppt, ppt->prompt, tgetnum("co"));
}

char				*prompt(t_sh *sh, char *prompt)
{
	char			inbuf[READ_LEN + 1];
	t_ppt			ppt;
	int				sel;

	g_prompt_static = &ppt;
	ft_bzero(inbuf, READ_LEN + 1);
	ft_bzero(&ppt, sizeof(t_ppt));
	put_prompt(prompt, &ppt);
	ppt.size = BUFFER_RADIX;
	if (!(ppt.line = ft_strnew(sizeof(char) * (ppt.size - 1))))
		internal_exit_wrapper(sh, MSH_ENOMEM);
	ft_bzero(ppt.line, ppt.size);
	while (!ppt.flag && sh->run
			&& (ppt.rd_ret = read(0, inbuf, READ_LEN)) > 0)
	{
		sel = MAX_INDEX(g_fkeyfuncs);
		while (sel > 0 && ft_strcmp(g_fkeyfuncs[sel].seq, inbuf))
			sel--;
		g_fkeyfuncs[sel].func(sh, &ppt, inbuf);
		ft_bzero(inbuf, READ_LEN + 1);
	}
	if (ppt.flag & ~RETURN_BIT)
		f_handle_flag(sh, &ppt);
	g_prompt_static = NULL;
	return (ppt.line ? ft_wind(ppt.line) : NULL);
}

void				ft_main_loop(t_sh *shell)
{
	char		*line;

	shell->history = generate_history(shell);
	while (shell->run)
	{
		/*line = prompt(shell, DEFAULT_PROMPT);*/
		line = prompt(shell, custom_prompt(shell));
		if (line)
		{
			if (hist2_add(shell, line))
				break ;
			exec_system(shell, &line);
		}
		if (line)
			ft_unwind(line);
		ft_magic_percent();
	}
	if (shell->history)
		dump_history(shell->history);
}
