#include <term.h>
#include "msh.h"

static void	f_do_backspace(t_sh *sh, t_ppt *ppt)
{
	size_t	len;

	hist2_trigger(sh, ppt);
	len = ft_strlen(ppt->line + ppt->idx);
	ft_memmove((void *)ppt->line + ppt->idx - 1,
				(void *)ppt->line + ppt->idx, len + 1);
	ppt->idx -= 1;
	ppt->mx -= 1;
}

void		fkey_backsp(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	int		ct;
	int		co;

	(void)sh;
	(void)inbuf;
	if (ppt->idx <= 0)
		return ;
	co = tgetnum("co");
	f_do_backspace(sh, ppt);
	tputs(tgetstr("ce", NULL), 1, ft_termput);
	if (iterate_hpos(ppt, 0, co) == 0)
		tputs(tgetstr("le", NULL), 1, ft_termput);
	ct = 0;
	ct += smart_put(ppt, ppt->line + ppt->idx, co);
	ct += smart_put(ppt, " ", co);
	while (ct-- > 0)
	{
		if (iterate_hpos(ppt, 0, co) == 0)
			tputs(tgetstr("le", NULL), 1, ft_termput);
	}
}
