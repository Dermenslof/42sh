#include <stdlib.h>
#include "msh.h"

t_term				*recall_term_data(int action)
{
	static t_term	*data = NULL;

	if (!(action < 0) && data == NULL)
	{
		if (!(data = (t_term *)malloc(sizeof(t_term))))
			return (NULL);
		ft_bzero(data, sizeof(t_term));
	}
	else if (action < 0)
	{
		free(data);
		data = NULL;
	}
	return (data);
}
