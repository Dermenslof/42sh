#include "msh.h"

static void		display_env2(t_tlist_node *base)
{
	t_var	*env;

	env = (t_var *)base;
	ft_putstr(env->key);
	ft_putstr("=");
	ft_putendl(env->value);
}

int				builtin_env(t_sh *shell, int ac, char **av)
{
	(void)ac;
	(void)av;
	ft_tlist_each(shell->env, display_env2);
	return (0);
}
