#include <stdlib.h>
#include <term.h>
#include <termios.h>
#include "msh.h"

static void		f_tune_termios(struct termios *sh)
{
	sh->c_lflag &= ~(ECHO | ICANON | ISIG);
	sh->c_cc[VMIN] = 1;
	sh->c_cc[VTIME] = 0;
}

void			init_term(t_term *term, t_tlist *env)
{
	ft_bzero(term, sizeof(t_term));
	if ((tcgetattr(0, &(term->rv))) < 0)
		exit(-1);
	term->sh = term->rv;
	term->rv.c_lflag |= ISIG;
	f_tune_termios(&(term->sh));
	term->tname = sh_getenv("TERM", env);
	if (ft_strcmp(term->tname, "") == 0)
		term->tname = FALLBACK_TERM_NAME;
	if (tgetent(NULL, NULL) < 1)
		exit(-1);
}
