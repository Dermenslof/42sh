#include <term.h>
#include "msh.h"

static int		f_discard_nonprintable(char *inbuf)
{
	if (*inbuf >= ' ')
		return (0);
	return (1);
}

static int		f_test_length(t_sh *sh, t_ppt *ppt)
{
	int			nu_len;

	nu_len = ppt->mx + ppt->rd_ret;
	if ((size_t)(nu_len) < ppt->size - 1)
		return (0);
	else
	{
		ppt->size += BUFFER_RADIX;
		if ((ppt->line = ft_realloc(ppt->line, ppt->size)) == NULL)
		{
			sh->run = 0;
			return (1);
		}
		ft_bzero(ppt->line + nu_len, ppt->size - (size_t)nu_len);
	}
	return (0);
}

void			fkey_nop(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	size_t		ins;
	int			co;
	int			i;

	if (f_discard_nonprintable(inbuf) || f_test_length(sh, ppt))
		return ;
	hist2_trigger(sh, ppt);
	co = tgetnum("co");
	ins = ft_strlen(inbuf);
	ft_memmove(ppt->line + ppt->idx + ins, ppt->line + ppt->idx,
				ft_strlen(ppt->line + ppt->idx));
	ft_memcpy(ppt->line + ppt->idx, inbuf, ins);
	smart_put(ppt, ppt->line + ppt->idx, co);
	ppt->idx += ppt->rd_ret;
	ppt->mx += ppt->rd_ret;
	i = ppt->mx;
	while (i-- > ppt->idx)
	{
		if (iterate_hpos(ppt, 0, co) == 0)
			tputs(tgetstr("le", NULL), 0, ft_termput);
	}
}
