#include <stdlib.h>
#include <term.h>
#include "msh.h"

static void			f_fake_destructor(t_tlist_node *node)
{
	(void)node;
}

int					hist_stash_push(t_sh *sh, t_ppt *ppt)
{
	t_history		*nu;
	char			*buf_cpy;

	if (ppt->hist_stash != NULL)
		return (0);
	if (ft_strlen(ppt->line) == 0)
		return (0);
	if ((buf_cpy = ft_strdup(ppt->line)) == NULL)
	{
		internal_exit_wrapper(sh, MSH_ENOMEM);
		return (-1);
	}
	if ((nu = hist_new_entry(buf_cpy)) == NULL)
	{
		internal_exit_wrapper(sh, MSH_ENOMEM);
		return (-1);
	}
	ft_tlist_push_back(sh->history, (t_tlist_node *)nu);
	ppt->hist_stash = nu;
	return (0);
}

char				*hist_stash_pop(t_sh *shell, t_ppt *ppt)
{
	char			*pop;

	pop = ppt->hist_stash->line;
	ft_tlist_remove(shell->history, (t_tlist_node *)ppt->hist_stash,
					f_fake_destructor);
	ppt->hist_stash = NULL;
	shell->hist_size -= (shell->hist_size > 0) ? 1 : 0;
	return (pop);
}

void				hist_update_prompt(t_ppt *ppt)
{
	int				co;

	co = tgetnum("co");
	while (ppt->vpos > 0)
	{
		tputs(tgetstr("up", NULL), 0, ft_termput);
		ppt->vpos--;
	}
	tputs(tgetstr("cd", NULL), 0, ft_termput);
	tputs(tgetstr("cr", NULL), 0, ft_termput);
	ppt->hpos = 0;
	smart_put(ppt, ppt->prompt, co);
	smart_put(ppt, ppt->line, co);
	tputs(tgetstr("ce", NULL), 0, ft_termput);
	ppt->mx = ft_strlen(ppt->line);
	ppt->idx = ppt->mx;
}

void				hist_enforce_size(t_sh *shell)
{
	if (shell->hist_size <= HISTMAX)
		return ;
	while (shell->hist_size > HISTMAX)
		ft_tlist_remove(shell->history, shell->history->elements, NULL);
}
