#include <term.h>
#include "msh.h"

void			fkey_right(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	(void)sh;
	(void)inbuf;
	if (ppt->idx >= ppt->mx)
		return ;
	ppt->idx += 1;
	if (iterate_hpos(ppt, 1, 0) == 0)
		tputs(tgetstr("nd", NULL), 1, ft_termput);
}
