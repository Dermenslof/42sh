#include <stdlib.h>
#include "msh.h"

void			hist2_trigger(t_sh *sh, t_ppt *ppt)
{
	if ((ppt->hist_ptr != NULL && ppt->hist_ptr == ppt->hist_stash)
			|| ppt->hist_stash != NULL)
	{
		free(hist_stash_pop(sh, ppt));
		ppt->hist_ptr = NULL;
	}
}

int				hist2_add(t_sh *sh, char *line)
{
	t_history	*nu;
	char		*cp;

	if (ft_strlen(line) == 0)
		return (0);
	nu = hist_new_entry(line);
	cp = ft_strdup(line);
	if (nu == NULL || cp == NULL)
	{
		internal_exit_wrapper(sh, MSH_ENOMEM);
		return (-1);
	}
	nu->line = cp;
	ft_tlist_push_back(sh->history, (t_tlist_node *)nu);
	hist_enforce_size(sh);
	return (0);
}

char			*hist2_dup_modulo_radix(char *s, size_t *p_sz)
{
	size_t		len;
	size_t		nulen;
	char		*cp;

	len = ft_strlen(s);
	nulen = len + 1;
	while (nulen % BUFFER_RADIX != 0)
		nulen++;
	if ((cp = (char *)ft_memalloc(nulen * sizeof(char))) == NULL)
		return (NULL);
	*p_sz = nulen;
	ft_memcpy(cp, s, len);
	return (cp);
}

int				hist2_rebuffer(t_sh *sh, t_ppt *ppt)
{
	char		*ret;

	if (ppt->hist_ptr == NULL)
		return (0);
	free(ppt->line);
	if (ppt->hist_ptr == ppt->hist_stash)
	{
		ret = hist_stash_pop(sh, ppt);
		ppt->hist_ptr = NULL;
		ppt->line = hist2_dup_modulo_radix(ret, &ppt->size);
		free(ret);
	}
	else
		ppt->line = hist2_dup_modulo_radix(ppt->hist_ptr->line, &ppt->size);
	if (ppt->line == NULL)
	{
		internal_exit_wrapper(sh, MSH_ENOMEM);
		return (-1);
	}
	return (0);
}
