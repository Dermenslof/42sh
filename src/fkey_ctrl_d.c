#include "msh.h"

/*
** Exit if the buffer is not empty (and there are not stopped / bg jobs)
*/

void		fkey_ctrl_d(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	(void)sh;
	(void)inbuf;
	if (ft_strlen(ppt->line) != 0)
		return ;
	ft_putendl("exit");
	ppt->flag |= EXIT_BIT;
}
