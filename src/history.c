#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "msh.h"

extern t_sh			g_shell;
static int			g_fd = -1;

static void			destroy_history(t_tlist_node *node)
{
	free(((t_history *)node)->line);
	((t_history *)node)->line = NULL;
	g_shell.hist_size -= 1;
}

t_history			*hist_new_entry(char *line)
{
	t_history		*history;

	if ((history = (t_history *)ft_memalloc(sizeof(t_history))))
		history->line = line;
	g_shell.hist_size += 1;
	return (history);
}

static void			write_history(t_tlist_node *node)
{
	ft_putendl_fd(((t_history *)node)->line, g_fd);
}

t_tlist				*generate_history(t_sh *shell)
{
	t_tlist			*history;
	char			*line;
	t_history		*node;
	char			*m;

	m = sh_getenv("HOME", shell->env);
	m = ft_strjoin(m, "/.msh_history");
	g_fd = open(m, O_RDONLY | O_CREAT, 0600);
	free(m);
	if (g_fd == -1)
		ft_putendl_fd("Error: unable to open/create history file.", 2);
	history = ft_tlist_new(destroy_history);
	while (ft_get_line(g_fd, &line) > 0)
	{
		if (line == NULL || (node = hist_new_entry(line)) == NULL)
		{
			internal_exit_wrapper(shell, MSH_ENOMEM);
			history = NULL;
			break ;
		}
		ft_tlist_push_back(history, (t_tlist_node *)node);
	}
	close(g_fd);
	g_fd = -1;
	return (history);
}

void				dump_history(t_tlist *history)
{
	char			*m;

	if (history->elements == NULL)
		return ;
	m = sh_getenv("HOME", g_shell.env);
	m = ft_strjoin(m, "/.msh_history");
	g_fd = open(m, O_WRONLY | O_CREAT | O_TRUNC, 0600);
	free(m);
	if (g_fd == -1)
	{
		ft_putendl_fd("Error: unable to write history.", 2);
		return ;
	}
	ft_tlist_each(history, write_history);
	close(g_fd);
	g_fd = -1;
	ft_tlist_delete(history);
}
