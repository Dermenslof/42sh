#include <stdlib.h>
#include <unistd.h>
#include "msh.h"

static char			*get_path(t_sh *shell)
{
	int			size_home;
	char		*home;

	home = sh_getenv("HOME", shell->env);
	size_home = ft_strlen(home);
	if (!ft_strcmp(home, shell->pwd))
		return (ft_strdup("~"));
	if (!ft_strncmp(shell->pwd, home, size_home))
		return (ft_strjoin("~", &shell->pwd[size_home]));
	return (ft_strdup(shell->pwd));
}

char				*custom_prompt(t_sh *shell)
{
	char		*prompt;
	char		*tmp1;
	char		*tmp2;
	char		*path;
	char		hostname[1024];

	gethostname(hostname, 1023);
	path = get_path(shell);
	tmp1 = ft_strdup("[\033[32;1m");
	tmp2 = ft_strjoin(tmp1, sh_getenv("USER", shell->env));
	free(tmp1);
	tmp1 = ft_strjoin(tmp2, "\033[0m@\033[35;1m");
	free(tmp2);
	tmp2 = ft_strjoin(tmp1, hostname);
	free(tmp1);
	tmp1 = ft_strjoin(tmp2, "\033[0m]");
	free(tmp2);
	tmp2 = ft_strjoin(tmp1, path);
	free(tmp1);
	free(path);
	prompt = ft_strjoin(tmp2, "> ");
	free(tmp2);
	return (prompt);
}
