#include <stdlib.h>
#include <glob.h>
#include "msh.h"

void			ft_quote(t_sh *shell, char **str, t_tok *tok)
{
	(*str)++;
	while (**str != '\'')
	{
		if (**str)
			*str += ft_append(tok, *str, 1);
		else
		{
			ft_append(tok, "\n", 1);
			ft_unwind(*str);
			*str = prompt(shell, "quote> ");
		}
	}
	(*str)++;
}

void			ft_dquote(t_sh *shell, char **str, t_tok *tok)
{
	(*str)++;
	while (**str != '"')
	{
		if (**str == '\\')
			ft_backslash(shell, str, tok);
		else if (**str == '$')
			ft_expand_env(str, shell, tok);
		else if (**str == 0)
		{
			ft_append(tok, "\n", 1);
			ft_unwind(*str);
			*str = prompt(shell, "dquote> ");
		}
		else
			*str += ft_append(tok, *str, 1);
	}
	(*str)++;
}
