#include <fcntl.h>
#include <stdlib.h>
#include "msh.h"

static void			handle_redir(t_tok *tok, t_prgm *p, int m)
{
	int				flags;
	int				fd;

	flags = 0;
	if (m == T_ROUT)
		flags = O_CREAT | O_TRUNC | O_WRONLY;
	else if (m == T_RIN)
		flags = O_RDONLY;
	else if (m == T_RAPP)
		flags = O_CREAT | O_WRONLY | O_APPEND;
	if ((flags & O_CREAT))
		fd = open(tok->str, flags, 0644);
	else
		fd = open(tok->str, flags);
	if (fd < 0)
	{
		ft_putstr_fd("42sh: Error dealing with file: ", 2);
		ft_putendl_fd(tok->str, 2);
		return ;
	}
	if (m != T_RIN)
		p->out = fd;
	else
		p->in = fd;
}

static void			pump_single(t_tok *tok, t_prgm *p, int *m)
{
	if (tok->type == T_OPERATOR)
		*m = (int)(tok->op);
	else if (tok->type == T_OPERAND)
	{
		if (*m == -1)
		{
			p->argc++;
			p->argv = ft_realloc(p->argv, sizeof(char *) * (p->argc + 1));
			p->argv[p->argc - 1] = tok->str;
			p->argv[p->argc] = NULL;
		}
		else
		{
			handle_redir(tok, p, *m);
			*m = -1;
		}
	}
}

static t_prgm		*pump_prgm(t_tlist *lst)
{
	t_prgm			*prgm;
	t_tok			*tmp;
	int				m;

	if (ft_tlist_is_empty(lst))
		return (NULL);
	prgm = malloc(sizeof(t_prgm));
	prgm->in = 0;
	prgm->out = 1;
	prgm->argc = 0;
	prgm->argv = malloc(sizeof(char *));
	*prgm->argv = NULL;
	tmp = ((t_tok *)lst->elements);
	m = -1;
	while (tmp && (tmp->type == T_OPERAND || tmp->type == T_OPERATOR))
	{
		if (tmp->op == T_AND || tmp->op == T_OR)
			break ;
		pump_single(tmp, prgm, &m);
		ft_tlist_remove(lst, (t_tlist_node *)tmp, NULL);
		tmp = ((t_tok *)lst->elements);
	}
	if (tmp && tmp->type == T_PIPE)
		ft_tlist_remove(lst, (t_tlist_node *)tmp, NULL);
	return (prgm);
}

static void			destructor_prgm(t_tlist_node *node)
{
	free_tab(((t_prgm *)node)->argv);
}

void				exec_system(t_sh *shell, char **s)
{
	t_tlist			*lst;
	t_tlist			*prgm;
	int				cond;

	lst = ft_tlist_new(NULL);
	prgm = ft_tlist_new(destructor_prgm);
	if (!lex(s, lst, shell))
	{
		ft_tlist_delete(prgm);
		ft_tlist_delete(lst);
		return ;
	}
	while (!ft_tlist_is_empty(lst) && ((t_tok *)lst->elements)->type != T_END)
	{
		cond = 0;
		check_cond(lst, &cond);
		while (lst->elements && continue_token(((t_tok *)lst->elements)))
			ft_tlist_push_back(prgm, (t_tlist_node *)pump_prgm(lst));
		if (lst->elements && ((t_tok *)(lst->elements))->type == T_SEP)
			ft_tlist_remove(lst, lst->elements, NULL);
		exec_cmd(shell, prgm, cond);
		ft_tlist_clear(prgm, NULL);
	}
	ft_tlist_delete(lst);
	ft_tlist_delete(prgm);
}
