#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include "msh.h"

static unsigned int		g_pid;

void		ft_link_pipe(t_tlist *prgm)
{
	t_prgm	*p;
	int		pip[2];

	p = ((t_prgm *)(prgm->elements));
	while (p && p->base.next)
	{
		pipe(pip);
		p->out = pip[1];
		((t_prgm *)p->base.next)->in = pip[0];
		p = ((t_prgm *)p->base.next);
	}
}

void		ft_exec_binary2(t_sh *shell, t_prgm *prgm)
{
	int		pid;

	if ((pid = fork()) == 0)
	{
		dup2(prgm->in, 0);
		dup2(prgm->out, 1);
		meta_init_revert(shell);
		close_every_fd(prgm);
		execve(prgm->argv[0], prgm->argv, env_to_arr(shell));
		exit(-1);
	}
	else
	{
		signal(SIGINT, SIG_IGN);
		prgm->pid = pid;
	}
}

static int	ft_equals_pid(t_tlist_node *prgm)
{
	if (((t_prgm *)prgm)->pid == g_pid)
		return (1);
	return (0);
}

t_prgm		*ft_get_pid(t_tlist *list, int pid)
{
	g_pid = pid;
	return (((t_prgm *)ft_tlist_search(list, ft_equals_pid)));
}

void		ft_close_fd(t_prgm *prgm)
{
	if (prgm->in > 2)
		close(prgm->in);
	if (prgm->out > 2)
		close(prgm->out);
}
