#include <unistd.h>
#include "msh.h"

/*
** To feed to tputs() .
*/

int				ft_termput(int d)
{
	char		c;

	c = *(char *)&d;
	return (write(1, &c, 1));
}

int				smart_put(t_ppt *ppt, char *line, int co)
{
	int			ct;

	ct = 0;
	while (*line)
	{
		write(1, line, 1);
		line += 1;
		iterate_hpos(ppt, 1, co);
		ct += 1;
	}
	return (ct);
}
