#include <unistd.h>
#include <term.h>
#include "msh.h"

void		ft_magic_percent(void)
{
	char	buf[32];
	char	*semicolon;

	ft_putstr("\033[6n");
	buf[read(0, buf, 31)] = 0;
	if (!(semicolon = ft_strchr(buf, ';')))
		return ;
	if (ft_atoi(semicolon + 1) != 1)
	{
		tputs(tgetstr("mr", NULL), 0, ft_termput);
		ft_putchar('%');
		tputs(tgetstr("me", NULL), 0, ft_termput);
		ft_putchar('\n');
	}
}
