#include <stdlib.h>
#include "libft.h"

char		**split_path(char *pwd, char *dir, char *dir2)
{
	char	**tab;
	int		i;

	i = 0;
	tab = ft_strsplit(pwd, '/');
	while (tab[i])
	{
		if (ft_strcmp(tab[i], dir) == 0)
		{
			free(tab[i]);
			tab[i] = ft_strdup(dir2);
			return (tab);
		}
		i++;
	}
	i = 0;
	while (tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab);
	return (NULL);
}

char		*generate_path(char **tab_path)
{
	char	*path;
	int		len;
	int		i;

	i = 0;
	len = 1;
	while (tab_path[i])
	{
		len += ft_strlen(tab_path[i]);
		i++;
	}
	path = (char *)ft_memalloc(len);
	i = 0;
	len = 1;
	path[0] = '/';
	while (tab_path[i])
	{
		ft_strcpy(&(path[len]), tab_path[i]);
		len += ft_strlen(tab_path[i]);
		path[len] = '/';
		len++;
		i++;
	}
	return (path);
}
