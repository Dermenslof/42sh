#include <term.h>
#include "msh.h"

static void			f_point_last_link(t_sh *sh, t_ppt *ppt)
{
	t_tlist_node	*p;

	p = sh->history->elements;
	if (p == NULL)
		return ;
	while (p->next)
		p = p->next;
	ppt->hist_ptr = (t_history *)p;
}

void				fkey_up(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	(void)inbuf;
	if (ppt->hist_ptr == NULL)
		f_point_last_link(sh, ppt);
	else if (ppt->hist_ptr->base.prev == NULL)
		return ;
	else if (ppt->hist_ptr->base.prev != NULL)
		ppt->hist_ptr = (t_history *)ppt->hist_ptr->base.prev;
	if (hist_stash_push(sh, ppt))
		return ;
	if (hist2_rebuffer(sh, ppt))
		return ;
	hist_update_prompt(ppt);
}
