#include <term.h>
#include <signal.h>
#include "msh.h"

static sig_t	g_static_sighandling[] =
{
	[SIGINT] = SIG_DFL,
	[SIGTSTP] = SIG_DFL,
};

static void		f_sigwinch_handler(int s)
{
	(void)s;
	tgetent(NULL, NULL);
}

int				signals_init_actions(void)
{
	static char init_flag = 0;

	if (init_flag)
	{
		signal(SIGINT, prompt_sigint_handler);
		signal(SIGTSTP, SIG_IGN);
	}
	else
	{
		signal(SIGWINCH, f_sigwinch_handler);
		g_static_sighandling[SIGINT] = signal(SIGINT, prompt_sigint_handler);
		g_static_sighandling[SIGTSTP] = signal(SIGTSTP, SIG_IGN);
		init_flag = 1;
	}
	return ((int)init_flag);
}

int				signals_revert_actions(void)
{
	signal(SIGINT, g_static_sighandling[SIGINT]);
	return (g_static_sighandling[SIGTSTP] == SIG_IGN);
}
