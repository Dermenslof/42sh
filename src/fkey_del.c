#include <term.h>
#include "msh.h"

void			fkey_del(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	size_t		len;
	int			co;
	int			ct;

	(void)sh;
	(void)inbuf;
	if (ppt->idx >= ppt->mx)
		return ;
	co = tgetnum("co");
	hist2_trigger(sh, ppt);
	len = ft_strlen(ppt->line + ppt->idx + 1);
	ft_memmove((void *)ppt->line + ppt->idx,
				(void *)ppt->line + ppt->idx + 1, len + 1);
	ppt->mx -= 1;
	tputs(tgetstr("ce", NULL), 1, ft_termput);
	ct = 0;
	ct += smart_put(ppt, ppt->line + ppt->idx, co);
	ct += smart_put(ppt, " ", co);
	while (ct-- > 0)
	{
		if (iterate_hpos(ppt, 0, co) == 0)
			tputs(tgetstr("le", NULL), 1, ft_termput);
	}
}
