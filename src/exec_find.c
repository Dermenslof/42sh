#include <unistd.h>
#include <stdlib.h>
#include "msh.h"

static char	*ft_fuse(char *path, char *bin)
{
	char	*str;

	str = ft_memalloc(ft_strlen(path) + ft_strlen(bin) + 2);
	ft_strcpy(str, path);
	ft_strcat(str, "/");
	ft_strcat(str, bin);
	return (str);
}

char		*ft_find_exec2(char **path, char *bin)
{
	int		i;
	char	*str;
	char	*v;

	str = NULL;
	i = 0;
	while (path[i] && !str)
	{
		v = ft_fuse(path[i], bin);
		if (!access(v, F_OK))
			str = v;
		else
			free(v);
		i++;
	}
	if (!str)
		ft_putendl_fd("Error: Command not found.", 2);
	return (str);
}

char		*ft_find_exec(char *bin, t_sh *shell)
{
	char	*path;
	char	**path_arr;
	char	*v;

	path = sh_getenv("PATH", shell->env);
	if (!path)
	{
		ft_putendl_fd("Error: PATH is not set, will not proceed.", 2);
		return (NULL);
	}
	path_arr = ft_strsplit(path, ':');
	if (!path_arr)
	{
		ft_putendl_fd("Error: PATH is not correct :(", 2);
		return (NULL);
	}
	v = ft_find_exec2(path_arr, bin);
	free_tab(path_arr);
	return (v);
}
