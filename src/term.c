#include <term.h>
#include "msh.h"

void			wrap_left(t_ppt *ppt, int co)
{
	char		*cap;
	int			i;

	tputs(tgetstr("up", NULL), 1, ft_termput);
	ppt->vpos -= 1;
	i = co - 1;
	if ((cap = tgetstr("ch", 0)) != NULL)
		tputs(tgoto(cap, 0, co - 1), 0, ft_termput);
	else
	{
		tputs(tgetstr("cr", NULL), 0, ft_termput);
		if ((cap = tgetstr("RI", NULL)) != NULL)
			tputs(tgoto(tgetstr("RI", NULL), 0, co - 1), 0, ft_termput);
		else
		{
			while (i-- > 0)
				tputs(tgetstr("nd", NULL), 1, ft_termput);
		}
	}
	ppt->hpos = co - 1;
}

void			wrap_right(t_ppt *ppt, int co)
{
	(void)co;
	tputs(tgetstr("do", NULL), 0, ft_termput);
	ppt->vpos += 1;
	tputs(tgetstr("cr", NULL), 0, ft_termput);
	ppt->hpos = 0;
}

int				iterate_hpos(t_ppt *ppt, char go_right, int co)
{
	int			way;
	char		wrapped;

	if (co <= 0)
		co = tgetnum("co");
	wrapped = 0;
	way = (go_right) ? +1 : -1;
	ppt->hpos += way;
	if (ppt->hpos < 0 || ppt->hpos >= co)
	{
		wrapped = 1;
		if (go_right)
		{
			wrap_right(ppt, co);
		}
		else
		{
			wrap_left(ppt, co);
		}
	}
	return ((int)wrapped);
}
