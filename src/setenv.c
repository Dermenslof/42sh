#include <stdlib.h>
#include "msh.h"

static int	ft_setenv2(t_sh *shell, int ac, char **av)
{
	t_var			*element;
	char			*key;
	char			*value;

	if (ac == 2)
		value = "";
	else
		value = av[2];
	key = av[1];
	if (ft_strchr(av[1], '=') != NULL)
	{
		ft_putendl("setenv: syntax error.");
		return (1);
	}
	element = search_env(key, shell->env);
	if (element == NULL)
	{
		element = create_var(key, value);
		return (!ft_tlist_push_back(shell->env, (t_tlist_node *)element));
	}
	free(element->value);
	element->value = ft_strdup(value);
	return (0);
}

int			builtin_setenv(t_sh *shell, int ac, char **av)
{
	if (ac == 1)
		return (builtin_env(shell, ac, av));
	else if (ac <= 3)
		return (ft_setenv2(shell, ac, av));
	else
		ft_putendl("setenv: too many arguments.");
	return (1);
}
