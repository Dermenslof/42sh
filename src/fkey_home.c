#include <term.h>
#include "msh.h"

void			fkey_home(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	int			co;

	(void)sh;
	(void)inbuf;
	co = tgetnum("co");
	while (ppt->idx > 0)
	{
		ppt->idx--;
		if (iterate_hpos(ppt, 0, co) == 0)
			tputs(tgetstr("le", NULL), 0, ft_termput);
	}
}
