#include <term.h>
#include "msh.h"

void			fkey_left(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	int			co;

	(void)sh;
	(void)inbuf;
	co = tgetnum("co");
	if (ppt->idx < 1)
		return ;
	ppt->idx -= 1;
	if (iterate_hpos(ppt, 0, co) == 0)
		tputs(tgetstr("le", NULL), 1, ft_termput);
}
