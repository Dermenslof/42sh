#include "msh.h"

static t_operator	g_ops[] =
{
	{T_RAPP, ">>"},
	{T_ROUT, ">"},
	{T_RIN, "<"},
	{T_AND, "&&"},
	{T_OR, "||"}
};

t_operator			*ft_get_operator(char *str)
{
	unsigned int	i;

	i = 0;
	while (i < sizeof(g_ops) / sizeof(g_ops[0]))
	{
		if (ft_strncmp(str, g_ops[i].str, ft_strlen(g_ops[i].str)) == 0)
			return (g_ops + i);
		i++;
	}
	return (NULL);
}

t_tok				*ft_grab_end(char **str)
{
	if (**str != 0)
		return (NULL);
	return (ft_tok_alloc());
}

t_tok				*ft_grab_operator(char **str)
{
	t_tok			*tok;
	t_operator		*op;

	op = ft_get_operator(*str);
	if (!op)
		return (NULL);
	(*str) += ft_strlen(op->str);
	tok = ft_tok_alloc();
	tok->op = op->op;
	return (tok);
}

t_tok				*ft_grab_pipe(char **str, t_sh *sh)
{
	if (**str == '|')
	{
		(*str)++;
		while (**str == ' ' || **str == '\t')
			(*str)++;
		if (**str == 0)
		{
			ft_unwind(*str);
			*str = prompt(sh, "pipe> ");
		}
		return (ft_tok_alloc());
	}
	return (0);
}

t_tok				*ft_grab_sep(char **str)
{
	if (**str == ';')
	{
		(*str)++;
		return (ft_tok_alloc());
	}
	return (0);
}
