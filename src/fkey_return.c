#include <stdlib.h>
#include <term.h>
#include "msh.h"

void		fkey_return(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	(void)sh;
	(void)inbuf;
	if (ppt->hist_stash)
		free(hist_stash_pop(sh, ppt));
	ppt->hist_ptr = NULL;
	ppt->flag = RETURN_BIT;
	ft_putchar('\n');
}
