#include "msh.h"

static t_builtin	g_builtin[] =
{
	{"cd", builtin_cd},
	{"echo", builtin_echo},
	{"env", builtin_env},
	{"exit", builtin_exit},
	{"export", builtin_export},
	{"meow", builtin_meow},
	{"pwd", builtin_pwd},
	{"setenv", builtin_setenv},
	{"unsetenv", builtin_unsetenv},
	{"forkbomb", builtin_forkbomb},
	{"matrix", builtin_matrix}
};

t_builtin_func		lookup_builtin(char *name)
{
	unsigned int	i;

	i = 0;
	while (i < (sizeof(g_builtin) / sizeof(t_builtin)))
	{
		if (ft_strcmp(name, g_builtin[i].name) == 0)
			return (g_builtin[i].func);
		i++;
	}
	return (NULL);
}
