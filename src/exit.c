#include <stdlib.h>
#include "msh.h"

int		builtin_exit(t_sh *shell, int ac, char **av)
{
	if (ac == 1)
		shell->run = 0;
	else if (ac == 2)
	{
		shell->run = 0;
		shell->exit_code = ft_atoi(av[1]);
	}
	else
	{
		ft_putendl("exit: too many arguments");
		return (1);
	}
	return (0);
}

void	internal_exit_wrapper(t_sh *shell, int code)
{
	shell->run = 0;
	shell->exit_code = code;
}
