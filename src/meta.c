#include <unistd.h>
#include <term.h>
#include "msh.h"

void			meta_init(t_sh *shell)
{
	t_tlist		*env;

	shell->run = 1;
	shell->exit_code = 0;
	shell->pwd = getcwd(NULL, FT_PATH_MAX);
	shell->oldpwd = ft_strdup(shell->pwd);
	list_environ(&env);
	init_term(&(shell->term), env);
	shell->env = env;
	add_last_ret(shell);
	i_update_environ(shell);
	signals_init_actions();
	if (tcsetattr(0, TCSAFLUSH, &shell->term.sh) < 0)
		internal_exit_wrapper(shell, 25);
}

void			meta_init_redo(t_sh *shell)
{
	if (tcsetattr(0, TCSAFLUSH, &shell->term.sh) < 0)
		internal_exit_wrapper(shell, 25);
	signals_init_actions();
	if (tgetent(NULL, NULL) < 1)
	{
		shell->run = 0;
		shell->exit_code = -1;
	}
}

void			meta_init_revert(t_sh *shell)
{
	if (tcsetattr(0, TCSAFLUSH, &shell->term.rv) < 0)
		internal_exit_wrapper(shell, 25);
	signals_revert_actions();
}
