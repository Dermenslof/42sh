#include <time.h>

/*
** Almost accurate !!
*/

void				ft_usleep(long sleeptime)
{
	struct timespec		rqtp;
	long				nano_i;

	rqtp.tv_sec = 0;
	sleeptime *= 1000L;
	while (sleeptime > 0L)
	{
		nano_i = (sleeptime >= 1000000000L) ? 999999999L : sleeptime;
		sleeptime -= nano_i;
		rqtp.tv_nsec = nano_i;
		if (nanosleep(&rqtp, NULL))
			break ;
	}
}

void				ft_sleep(unsigned int sleeptime)
{
	ft_usleep((long)sleeptime * 1000000L);
}
