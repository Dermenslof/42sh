#include <stdlib.h>
#include "msh.h"

t_var		*create_var(const char *key, const char *value)
{
	t_var	*node;

	node = (t_var *)malloc(sizeof(t_var));
	if (!node)
		return (NULL);
	node->key = ft_strdup(key);
	node->value = ft_strdup(value);
	return (node);
}

void		destroy_var(t_tlist_node *node)
{
	free(((t_var *)node)->key);
	free(((t_var *)node)->value);
}
