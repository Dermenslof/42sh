#include "msh.h"

int					builtin_pwd(t_sh *shell, int ac, char **av)
{
	(void)av;
	if (ac > 1)
	{
		ft_putendl_fd("Too many arguments.", 2);
		return (1);
	}
	ft_putendl(shell->pwd);
	return (0);
}
