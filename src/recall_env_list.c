#include <stdlib.h>
#include "libft.h"
#include "msh.h"

t_tlist					*recall_env_list(int action)
{
	static t_tlist		*list = NULL;

	if (!(action < 0) && list == NULL)
	{
		if (!(list = ft_tlist_new(destroy_var)))
			return (NULL);
		ft_bzero(list, sizeof(t_tlist));
	}
	else if (action < 0)
	{
		ft_tlist_delete(list);
		list = NULL;
	}
	return (list);
}
