#include "msh.h"

static char const	*g_query = NULL;

static int			f_test_key(t_tlist_node *node)
{
	return (ft_strcmp(g_query, ((struct s_var *)node)->key) == 0);
}

t_var				*search_env(char const *query, t_tlist *env)
{
	t_tlist_node	*ret;

	g_query = query;
	ret = ft_tlist_search(env, f_test_key);
	g_query = NULL;
	return ((t_var *)ret);
}

char				*sh_getenv(char const *query, t_tlist *env)
{
	t_var			*v;

	v = search_env(query, env);
	if (v)
		return (v->value);
	return ("");
}
