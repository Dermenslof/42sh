#include "msh.h"

int					fill_var(char *entry, t_var *var)
{
	char			*equal_char;
	int				diff;

	equal_char = ft_strchr(entry, '=');
	if (equal_char == NULL)
		equal_char = entry;
	diff = equal_char - entry;
	var->key = ft_strnew(diff);
	ft_strncpy(var->key, entry, diff);
	var->value = ft_strdup(equal_char + 1);
	return (0);
}
