#include <stdlib.h>
#include "msh.h"

int					ft_append(t_tok *tok, char *str, int n)
{
	char			*tmp;

	if (n == -1)
		n = ft_strlen(str);
	tmp = ft_realloc(tok->str, ft_strlen(tok->str) + n + 1);
	ft_strncat(tmp, str, n);
	tok->str = tmp;
	return (n);
}

void				ft_expand_env(char **str, t_sh *shell, t_tok *tok)
{
	char			*var_name;
	char			*var;
	int				i;

	i = 0;
	(*str)++;
	var_name = ft_strdup("");
	while (**str && (ft_isalnum(**str) || ft_strchr("?$_", **str)))
	{
		i++;
		var_name = ft_realloc(var_name, i + 1);
		var_name[i - 1] = **str;
		var_name[i] = 0;
		(*str)++;
	}
	var = sh_getenv(var_name, shell->env);
	if (var)
		ft_append(tok, var, -1);
	if (var_name)
		free(var_name);
}

void				ft_backslash(t_sh *shell, char **str, t_tok *tok)
{
	(*str)++;
	if (**str)
	{
		ft_append(tok, *str, 1);
		(*str)++;
	}
	else
		*str = prompt(shell, "> ");
}

static void			ft_char(char **str, t_sh *shell, t_tok *tok)
{
	if (**str == '$')
		ft_expand_env(str, shell, tok);
	else if (**str == '~')
	{
		ft_append(tok, sh_getenv("HOME", shell->env), -1);
		(*str)++;
	}
	else if (**str == '\\')
		ft_backslash(shell, str, tok);
	else if (**str == '\'')
		ft_quote(shell, str, tok);
	else if (**str == '"')
		ft_dquote(shell, str, tok);
	else
		(*str) += ft_append(tok, *str, 1);
}

t_tok				*ft_grab_operand(char **str, t_sh *shell)
{
	t_tok			*tok;

	tok = ft_tok_alloc();
	tok->str = ft_strdup("");
	while (**str && **str != ' ' && !ft_get_operator(*str))
		ft_char(str, shell, tok);
	return (tok);
}
