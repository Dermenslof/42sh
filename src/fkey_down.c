#include <term.h>
#include "msh.h"

static void		f_blank_prompt(t_sh *sh, t_ppt *ppt)
{
	char		*ret;

	if ((ret = hist2_dup_modulo_radix("", &ppt->size)) == NULL)
	{
		internal_exit_wrapper(sh, MSH_ENOMEM);
		return ;
	}
	ft_strdel(&ppt->line);
	ppt->line = ret;
	hist_update_prompt(ppt);
}

void			fkey_down(t_sh *sh, t_ppt *ppt, char *inbuf)
{
	(void)inbuf;
	if (ppt->hist_ptr == NULL || ppt->hist_stash == NULL
		|| ppt->hist_ptr->base.next == NULL)
	{
		f_blank_prompt(sh, ppt);
		return ;
	}
	else if (ppt->hist_ptr->base.next != NULL)
		ppt->hist_ptr = (t_history *)ppt->hist_ptr->base.next;
	if (hist2_rebuffer(sh, ppt))
		return ;
	hist_update_prompt(ppt);
}
