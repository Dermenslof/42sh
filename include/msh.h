#ifndef MSH_H
# define MSH_H

# include <termios.h>
# include "libft.h"

# define RECALL_GET				(1)
# define RECALL_FREE			(-1)

# define FT_PATH_MAX			(1024)
# define READ_LEN				(6)
# define FALLBACK_TERM_NAME		"vt100"
# define DEFAULT_PROMPT			"Meow $ "
# define BUFFER_RADIX			(128LU)
# define MAX_ARG_LEN			8192

# define SQ_NOP					""
# define SQ_CTRL_C				"\003"
# define SQ_CTRL_D				"\004"
# define SQ_RETURN				"\012"
# define SQ_BACKSP				"\177"
# define SQ_BS					"\010"
# define SQ_DEL					"\033[3~"
# define SQ_HOME				"\033[H"
# define SQ_SOH					"\001"
# define SQ_END					"\033[F"
# define SQ_UP					"\033[A"
# define SQ_DOWN				"\033[B"
# define SQ_LEFT				"\033[D"
# define SQ_RIGHT				"\033[C"

# define MAX_INDEX(array)		(sizeof((array)) / sizeof(*(array)) - 1)

# define RETURN_BIT				(0x1)
# define DISCARD_BIT			(0x2)
# define EXIT_BIT				(0x4)

# define MSH_ENOMEM				(12)

# define S_ERR(a)				((a) + 256)
# define S_END					(-1)

# define HISTMAX				(100)

typedef enum					e_keyval
{
	NOP = 0,
	CTRL_C,
	CTRL_D,
	RETURN,
	BACKSP,
	BS,
	DEL,
	HOME,
	SOH,
	END,
	UP,
	DOWN,
	LEFT,
	RIGHT,
	E_LEN
}								t_keyval;

typedef enum					e_token_type
{
	T_OPERAND = 0,
	T_OPERATOR = 1,
	T_PIPE = 2,
	T_SEP = 3,
	T_END = 4
}								t_token_type;

typedef enum					e_token_op
{
	T_BAD,
	T_ROUT,
	T_RAPP,
	T_RIN,
	T_OR,
	T_AND
}								t_token_op;

typedef struct					s_operator
{
	t_token_op					op;
	char						*str;
}								t_operator;

typedef struct					s_term
{
	struct termios				rv;
	struct termios				sh;
	char						*tname;
}								t_term;

typedef struct					s_var
{
	t_tlist_node				base;
	char						*key;
	char						*value;
}								t_var;

typedef struct					s_history
{
	t_tlist_node				base;
	char						*line;
}								t_history;

typedef struct					s_sh
{
	t_term						term;
	t_tlist						*history;
	int							hist_size;
	t_tlist						*var;
	t_tlist						*env;
	char						*pwd;
	char						*oldpwd;
	int							run;
	int							exit_code;
}								t_sh;

typedef int						(*t_builtin_func)(t_sh *, int, char **);

typedef struct					s_builtin
{
	char						*name;
	t_builtin_func				func;
}								t_builtin;

typedef struct					s_ppt
{
	char						*prompt;
	int							prompt_len;
	int							hpos;
	int							vpos;
	int							rd_ret;
	int							idx;
	int							mx;
	char						*line;
	size_t						size;
	char						flag;
	t_history					*hist_ptr;
	t_history					*hist_stash;
}								t_ppt;

typedef void					(*t_key_func)(t_sh *, t_ppt *, char *);

typedef struct					s_key
{
	char						*seq;
	t_key_func					func;
}								t_key;

typedef struct					s_tok
{
	t_tlist_node				base;
	t_token_type				type;
	t_token_op					op;
	char						*str;
}								t_tok;

typedef struct					s_prgm
{
	t_tlist_node				base;
	int							in;
	int							out;
	int							argc;
	char						**argv;
	unsigned int				pid;
}								t_prgm;

/*
**	pwd.c
*/

int								builtin_pwd(t_sh *sh, int ac, char **av);

/*
**	built_in.c
*/

t_builtin_func					lookup_builtin(char *name);

/*
**	var.c
*/

t_var							*create_var(const char *key, const char *value);
void							destroy_var(t_tlist_node *node);

/*
**	env.c
*/

int								builtin_env(t_sh *sh, int ac, char **av);

/*
**	setenv.c
*/

int								builtin_setenv(t_sh *sh, int ac, char **av);

/*
**	unset_env.c
*/

int								builtin_unsetenv(t_sh *sh, int ac, char **av);

/*
**	echo.c
*/

int								builtin_echo(t_sh *sh, int ac, char **av);

/*
**	prompt.c
*/

void							ft_main_loop(t_sh *sh);
char							*prompt(t_sh *sh, char *prompt);
void							prompt_sigint_handler(int foo);
void							put_ppt(char *prompt, t_ppt *ppt);

/*
** custom_prompt.c
*/

char							*custom_prompt(t_sh *shell);

/*
** prompt_history.c
*/

void							hist_update_prompt(t_ppt *ppt);
int								hist_stash_push(t_sh *sh, t_ppt *ppt);
char							*hist_stash_pop(t_sh *sh, t_ppt *ppt);
void							hist_enforce_size(t_sh *sh);

/*
** prompt_history2.c
*/

void							hist2_trigger(t_sh *sh, t_ppt *ppt);
int								hist2_add(t_sh *sh, char *line);
int								hist2_rebuffer(t_sh *sh, t_ppt *ppt);
char							*hist2_dup_modulo_radix(char *s, size_t *p_sz);

/*
** term.c
*/

void							wrap_left(t_ppt *ppt, int co);
void							wrap_right(t_ppt *ppt, int co);
int								iterate_hpos(t_ppt *ppt, char go_right, int co);

/*
**	fkey_*.c
*/

void							fkey_nop(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_ctrl_c(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_ctrl_d(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_backsp(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_del(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_down(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_end(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_home(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_left(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_return(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_right(t_sh *sh, t_ppt *ppt, char *inbuf);
void							fkey_up(t_sh *sh, t_ppt *ppt, char *inbuf);

/*
** termput.c
*/

int								ft_termput(int d);
int								smart_put(t_ppt *ppt, char *line, int co);

/*
**	cd.c
*/

int								builtin_cd(t_sh *sh, int ac, char **av);

/*
**	cd2.c
*/

char							**split_path(char *pwd, char *dir, char *dir2);
char							*generate_path(char **tab_path);

/*
**	fill_var.c
*/

int								fill_var(char *entry, t_var *var);

/*
**	list_environ.c
*/

void							add_last_ret(t_sh *shell);
int								list_environ(t_tlist **p_env);

/*
**	recall_*_*.c
*/

t_tlist							*recall_env_list(int action);
t_term							*recall_term_data(int action);

/*
**  meta.c
*/

void							meta_init(t_sh *sh);
void							meta_init_revert(t_sh *sh);
void							meta_init_redo(t_sh *sh);

/*
**	init_term.c
*/

void							init_term(t_term *term, t_tlist *env);

/*
** signals.c
*/

int								signals_init_actions(void);
int								signals_revert_actions(void);

/*
**	search_env.c
*/

t_var							*search_env(char const *query, t_tlist *env);
char							*sh_getenv(char const *query, t_tlist *env);

/*
** i_update_environ.c
*/

int								i_update_environ(t_sh *sh);

/*
**	exit.c
*/

int								builtin_exit(t_sh *sh, int ac, char **av);
void							internal_exit_wrapper(t_sh *sh, int code);

/*
**  lex.c
*/

t_tok							*ft_tok_alloc(void);
int								lex(char **str, t_tlist *list, t_sh *sh);

/*
**  lexer.c
*/

t_operator						*ft_get_operator(char *str);
t_tok							*ft_grab_end(char **str);
t_tok							*ft_grab_operator(char **str);
t_tok							*ft_grab_pipe(char **str, t_sh *sh);
t_tok							*ft_grab_sep(char **str);

/*
**  lexer2.c
*/

int								ft_append(t_tok *tok, char *str, int n);
void							ft_expand_env(char **str, t_sh *sh, t_tok *tok);
void							ft_backslash(t_sh *sh, char **str, t_tok *tok);
t_tok							*ft_grab_operand(char **str, t_sh *sh);

/*
**  lexer3.c
*/

void							ft_quote(t_sh *sh, char **str, t_tok *tok);
void							ft_dquote(t_sh *sh, char **str, t_tok *tok);

/*
**  export.c
*/

int								builtin_export(t_sh *sh, int ac, char **av);

/*
**  exec.c
*/

void							exec_system(t_sh *sh, char **s);

/*
**  exec2.c
*/

char							**env_to_arr(t_sh *sh);
void							exec_cmd(t_sh *sh, t_tlist *prgm, int cond);

/*
**  exec3.c
*/

t_prgm							*ft_get_pid(t_tlist *list, int pid);
void							ft_link_pipe(t_tlist *prgm);
void							ft_exec_binary2(t_sh *sh, t_prgm *prgm);
void							ft_close_fd(t_prgm *prgm);

/*
**  exec4.c
*/

int								continue_token(t_tok *tok);
void							check_cond(t_tlist *lst, int *cond);
void							ft_terminate(t_tlist_node *n);
void							close_every_fd(t_prgm *prgm);

/*
**  exec_find.c
*/

char							*ft_find_exec(char *bin, t_sh *sh);

/*
**	meow.c
*/

int								builtin_meow(t_sh *sh, int ac, char **av);

/*
**	utils.c
*/

void							free_tab(char **ptr);

/*
**	dumb.c
*/

int								builtin_forkbomb(t_sh *sh, int ac, char **av);
int								builtin_matrix(t_sh *sh, int ac, char **av);

/*
**	history.c
*/

t_history						*hist_new_entry(char *line);
t_tlist							*generate_history(t_sh *sh);
void							dump_history(t_tlist *history);

/*
** sleep.c
*/

void							ft_usleep(unsigned int sleeptime);
void							ft_sleep(unsigned int sleeptime);

/*
**  magic.c
*/

void							ft_magic_percent(void);

/*
**  wind.c
*/

char							*ft_wind(char *str);
void							ft_unwind(char *str);

#endif
