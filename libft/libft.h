#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t len);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
size_t				ft_strlen(const char *s);
char				*ft_strdup(const char *s1);
char				*ft_strcpy(char *s1, const char *s2);
char				*ft_strncpy(char *s1, const char *s2, size_t n);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_atoi(const char *str);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);
char				*ft_itoa(int n);
void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putnbr(int n);
void				ft_putchar_fd(char s, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);

/*
** Do NOT use these functions.
** Use the new ones that work with templates instead.
*/

t_list				*ft_lstnew(void const *a, size_t n);
void				ft_lstdelone(t_list **l, void (*f)(void *, size_t));
void				ft_lstdel(t_list **l, void (*f)(void *, size_t));
void				ft_lstadd(t_list **l, t_list *a);
void				ft_lstiter(t_list *l, void (*f)(t_list *));
t_list				*ft_lstmap(t_list *l, t_list *(*f)(t_list *));

void				*ft_realloc(void *a, size_t n);
int					ft_get_line(int n, char **l);

/*
** List Templates
*/

typedef struct		s_tlist_node
{
	struct s_tlist_node	*next;
	struct s_tlist_node	*prev;
}					t_tlist_node;

typedef void		(*t_tlist_func)(t_tlist_node *);
typedef void		(*t_tlist_dtor)(t_tlist_node *);
typedef int			(*t_tlist_pred)(t_tlist_node *);
typedef int			(*t_tlist_cmp)(t_tlist_node *, t_tlist_node *);

typedef struct		s_tlist
{
	t_tlist_node	*elements;
	t_tlist_func	dtor;
}					t_tlist;

t_tlist				*ft_tlist_new(t_tlist_dtor a);
int					ft_tlist_push_front(t_tlist *a, t_tlist_node *b);
int					ft_tlist_push_back(t_tlist *a, t_tlist_node *b);
void				ft_tlist_each(t_tlist *a, t_tlist_func b);
void				ft_tlist_each_if(t_tlist *a, t_tlist_pred b,
		t_tlist_func c);
t_tlist_node		*ft_tlist_search(t_tlist *a, t_tlist_pred b);
void				ft_tlist_swap(t_tlist *a, t_tlist_node *b,
		t_tlist_node *c);
void				ft_tlist_sort(t_tlist *a, t_tlist_cmp b);
void				ft_tlist_rsort(t_tlist *a, t_tlist_cmp b);
size_t				ft_tlist_size(t_tlist *a);
int					ft_tlist_is_empty(t_tlist *a);
void				ft_tlist_remove(t_tlist *a, t_tlist_node *b,
		t_tlist_dtor c);
void				ft_tlist_remove_if(t_tlist *a, t_tlist_pred b,
		t_tlist_dtor c);
void				ft_tlist_clear(t_tlist *a, t_tlist_dtor b);
void				ft_tlist_delete(t_tlist *a);

#endif

