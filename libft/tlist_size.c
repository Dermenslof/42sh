#include "libft.h"

size_t				ft_tlist_size(t_tlist *lst)
{
	size_t			size;
	t_tlist_node	*elem;

	elem = lst->elements;
	size = 0;
	while (elem)
	{
		size += 1;
		elem = elem->next;
	}
	return (size);
}
