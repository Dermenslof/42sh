#include "libft.h"

void					ft_tlist_each(t_tlist *lst_obj, t_tlist_func apply)
{
	t_tlist_node		*elem;

	elem = lst_obj->elements;
	while (elem)
	{
		apply(elem);
		elem = elem->next;
	}
}
