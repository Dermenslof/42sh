#include "libft.h"

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;

	str = ft_strnew(len);
	if (str)
	{
		ft_memcpy((void *)str, (void *)(s + start), len);
	}
	return (str);
}
