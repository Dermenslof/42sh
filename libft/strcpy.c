#include "libft.h"

char		*ft_strcpy(char *s1, const char *s2)
{
	return (ft_strncpy(s1, s2, ft_strlen(s2) + 1));
}
