#include <stdlib.h>
#include "libft.h"

void		*ft_realloc(void *data, size_t size)
{
	void	*ptr;

	ptr = NULL;
	if (size)
	{
		ptr = malloc(size);
		if (ptr)
			ft_memcpy(ptr, data, size);
	}
	free(data);
	data = NULL;
	return (ptr);
}
