#include <stdlib.h>
#include "libft.h"

void				ft_tlist_clear(t_tlist *lst, t_tlist_dtor dtor)
{
	t_tlist_node	*node;
	t_tlist_node	*nxt;

	node = lst->elements;
	while (node)
	{
		nxt = node->next;
		ft_tlist_remove(lst, node, dtor);
		node = nxt;
	}
}
