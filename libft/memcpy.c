#include <string.h>

void		*ft_memcpy(void *dst, const void *src, size_t n)
{
	void	*odst;

	odst = dst;
	while (n)
	{
		*((char*)dst) = *((char*)src);
		dst = (void*)((char*)dst + 1);
		src = (void*)((char*)src + 1);
		n--;
	}
	return (odst);
}
