#include "libft.h"

int					ft_tlist_push_back(t_tlist *list, t_tlist_node *node)
{
	t_tlist_node	*elem;

	if (list == NULL)
		return (-1);
	if (!list->elements)
		return (ft_tlist_push_front(list, node));
	elem = list->elements;
	while (elem->next)
		elem = elem->next;
	elem->next = node;
	node->prev = elem;
	node->next = NULL;
	return (1);
}
