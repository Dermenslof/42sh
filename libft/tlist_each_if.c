#include "libft.h"

void				ft_tlist_each_if(t_tlist *l, t_tlist_pred p, t_tlist_func f)
{
	t_tlist_node	*elem;

	elem = l->elements;
	while (elem)
	{
		if (p(elem))
			f(elem);
		elem = elem->next;
	}
}
