#include "libft.h"

int			ft_tlist_push_front(t_tlist *list, t_tlist_node *node)
{
	if (list == NULL)
		return (-1);
	if (list->elements)
		list->elements->prev = node;
	node->next = list->elements;
	node->prev = NULL;
	list->elements = node;
	return (1);
}
