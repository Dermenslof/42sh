#include <stdlib.h>
#include "libft.h"

void				ft_tlist_remove_if(t_tlist *lst,
										t_tlist_pred pred,
										t_tlist_dtor dtor)
{
	t_tlist_node	*node;
	t_tlist_node	*nxt;

	node = lst->elements;
	while (node)
	{
		nxt = node->next;
		if (pred(node))
			ft_tlist_remove(lst, node, dtor);
		node = nxt;
	}
}
