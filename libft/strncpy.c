#include <string.h>

char		*ft_strncpy(char *s1, const char *s2, size_t n)
{
	char	*dst;

	dst = s1;
	while (n)
	{
		*dst = *s2;
		dst++;
		if (*s2)
		{
			s2++;
		}
		n--;
	}
	return (s1);
}
