#include <stdlib.h>
#include "libft.h"

void			ft_tlist_delete(t_tlist *list)
{
	ft_tlist_clear(list, NULL);
	free(list);
}
