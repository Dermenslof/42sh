#include <stdlib.h>
#include "libft.h"

t_tlist			*ft_tlist_new(t_tlist_dtor destructor)
{
	t_tlist		*fresh;

	if (!(fresh = (t_tlist *)malloc(sizeof(t_tlist))))
		return (NULL);
	fresh->elements = NULL;
	fresh->dtor = destructor;
	return (fresh);
}
