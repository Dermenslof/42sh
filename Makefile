.PHONY: all, clean, fclean, re, debug

CC := $(shell which clang)
CFLAGS = -Wall -Wextra -Werror
LDFLAGS = -L libft -lft -ltermcap
FILES = built_in.c cd.c cd2.c echo.c env.c exec.c exec2.c exit.c export.c \
		fill_var.c termput.c meta.c meow.c prompt.c signals.c \
		init_term.c fkey_nop.c lex.c lexer.c lexer2.c list_environ.c main.c \
		pwd.c recall_env_list.c lexer3.c\
		recall_term_data.c search_env.c setenv.c \
		unset_env.c var.c fkey_backsp.c fkey_del.c fkey_down.c fkey_end.c \
		fkey_home.c fkey_left.c fkey_return.c fkey_right.c fkey_up.c \
		fkey_ctrl_c.c fkey_ctrl_d.c term.c i_update_environ.c exec_find.c \
		utils.c dumb.c sleep.c exec3.c exec4.c magic.c \
		history.c prompt_history.c prompt_history2.c matrix.c wind.c \
		custom_prompt.c
SRC = $(addprefix $(FILES), src/)
OBJ = $(FILES:%.c=obj/%.o)
NAME = 42sh

ifeq "$(CC)" ""
	CC := gcc
endif

ifeq ($(FAST), 1)
	CFLAGS := -O3 $(CFLAGS)
else
	CFLAGS := -O2 $(CFLAGS)
endif


all: $(NAME)

$(NAME): $(OBJ) libft/libft.a
	@echo "\033[35;1m• \033[0mCreating executable $@ ...\033[55G\c"
	@export result="`$(CC) $(CFLAGS) $(OBJ) $(LDFLAGS) -o $@ 2>&1`" ; \
	if [ "$$result" = "" ] ; then \
		echo "\033[37;1m[\033[32mOK !\033[37m]\033[0m" ; \
	else \
		echo "\033[37;1m[\033[31mFAIL\033[37m]\033[0m" ; \
		echo "\n$$result" ; \
		exit 1 ; \
	fi

ifeq ($(FAST), 1)

%.o : %_fast.c libft.h
	@echo "\033[35;1m• \033[0mCreating object file $@ ...\033[55G\c"
	@export result="`$(CC) $(CFLAGS) -I include -I libft -c $< -o $@ 2>&1`" ; \
	if [ "$$result" = "" ] ; then \
		echo "\033[37;1m[\033[32mOK !\033[37m]\033[0m" ; \
	else \
		echo "\033[37;1m[\033[31mFAIL\033[37m]\033[0m" ; \
		echo "\n$$result" ; \
		exit 1 ; \
	fi
endif

obj/%.o: src/%.c include
	@mkdir -p obj/
	@echo "\033[35;1m• \033[0mCreating object file $@ ...\033[55G\c"
	@export result="`$(CC) $(CFLAGS) -I include -I libft -c $< -o $@ 2>&1`" ; \
	if [ "$$result" = "" ] ; then \
		echo "\033[37;1m[\033[32mOK !\033[37m]\033[0m" ; \
	else \
		echo "\033[37;1m[\033[31mFAIL\033[37m]\033[0m" ; \
		echo "\n$$result" ; \
		exit 1 ; \
	fi

fast:
	@make -C . FAST=1

clean:
	@echo "\033[35;1m• \033[0mRemoving object files ...\033[55G\c"
	@rm -rf obj
	@echo "\033[37;1m[\033[32mOK !\033[37m]\033[0m"

fclean: clean
	@echo "\033[35;1m• \033[0mRemoving $(NAME) ...\033[55G\c"
	@rm -rf $(NAME)
	@echo "\033[37;1m[\033[32mOK !\033[37m]\033[0m"

re: fclean all

debug:
	@make -C . DEBUG=1

libft/libft.a:
	@make -C libft

